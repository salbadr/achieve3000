'use strict';

var express=require('express'),
    path=require('path'),
    app=express(),
    rootPath=path.normalize(__dirname+'/.'),
    publicPath=rootPath+'/public';
    
app.use(express.static(publicPath));

app.get('*', function(req, res){
    res.sendFile(publicPath+'/index.html');
    
});


app.listen(process.env.PORT, process.env.IP);

console.log('Server listening at port '+process.env.PORT+' ....');
    