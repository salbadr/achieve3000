(function (){
   'use strict';
   
   /**
    * 
    * The main app. Loads dependent modules
    * 
    * */
   angular.module('achieveApp',[
      /**
       * Core modules
       * 
       * */
      
      'achieveApp.core',
      
      /**
       * 
       * Common Services
       * 
       * */
      
      'achieveApp.common',
      /**
       * Features
       * */
      'achieveApp.newsletter'
      ]);
     
    
})();