(function (){
   'use strict';
   
   /**
    * 
    * Loads the core services needed for the App
    * 
    * */
   angular.module('achieveApp.core',['ngRoute']);
    
    
})();