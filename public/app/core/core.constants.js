(function() {
   'use strict';


   /**
    * 
    * Contains all the constants to be used in the App
    * 
    * */
   var core = angular.module('achieveApp.core');

   core.constant('MAIL_CHIMP_API_KEY', '7b6b7220d75c52bbed85c4114e6755b7');
   core.constant('MAIL_CHIMP_LIST_ID', '7c7be738af');

   core.constant('MAIL_CHIMP_API_URL', 'https://cors-anywhere.herokuapp.com/https://us4.api.mailchimp.com/2.0/');


})();