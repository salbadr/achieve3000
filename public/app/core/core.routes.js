(function() {
   'use strict';


   var core = angular.module('achieveApp.core');

   /**
    * 
    * Setting up the routes for the App
    * 
    * */
   core.config(
      function($routeProvider, $locationProvider) {
         $routeProvider.when('/', {
            templateUrl: 'app/newsletter/newsletter.html'
         });
         $routeProvider.when('/thankyou', {
            templateUrl: 'app/newsletter/thankyou.html'
         });

         $locationProvider.html5Mode(true);


      });
})();