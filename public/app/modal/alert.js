(function() {
    'use strict';

    /**
     * 
     * A simple controller for the Alert modal window
     * 
     * */
    angular.module('achieveApp.common').controller('Alert', ['$scope', '$uibModalInstance', Alert]);

    function Alert($scope, $uibModalInstance) {
        /**
         * 
         * Closes the modal window when ok is clicked
         * 
         * */
        $scope.ok = function() {
            $scope.$close();
        }
    }

})();