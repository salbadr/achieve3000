(function() {
    'use strict';

    angular.module('achieveApp.newsletter').controller('NewsletterForm', ['$scope', '$location', 'modal', 'mailChimp', NewsletterForm]);


    function NewsletterForm($scope, $location, modal, mailChimp) {



        /**
         * 
         * Submits the form to the mailChimp service. If successful then redirects to thankyou page
         * Otherwise shows a modal window so they can try again
         * 
         * @param Object user The user object containing their data
         * @param Object newsletterForm The newsletter form
         * 
         * 
         * */

        $scope.submitForm = function(user, newsletterForm) {



            if (!newsletterForm.$invalid) {

                mailChimp.subscribeUser(user).$promise.
                then(function(response) {
                    //redirect to thank you page
                    $location.path('/thankyou');
                }).
                catch(function(response) {
                    //show modal alert
                    modal.showAlert('alert');

                });

            }

        }

    }

})();