(function() {
    'use strict';

    /**
     * 
     * Handles showing of modal windows
     * 
     * */
    var modal = angular.module('achieveApp.common');

    modal.factory('modal', ['$uibModal', function mailChimp($uibModal) {

        /**
         * 
         * The public API
         * 
         **/
        var api = {
            showAlert: showAlert
        }

        return api;

        /**
         * 
         * Creates a modal bootstrap pop-up specified by filename
         * 
         * @param String filename The name of the file
         * @returns nothing
         * 
         * */
        function showAlert(filename) {

            var ucfirst = filename.charAt(0).toUpperCase() + filename.slice(1);

            $uibModal.open({
                animation: true,
                templateUrl: 'app/modal/' + filename + '.html',
                controller: ucfirst
            });

        }

    }]);

})();