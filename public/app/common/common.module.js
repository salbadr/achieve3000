(function (){
   'use strict';
   
   /**
    * 
    * Creates a common module to be used for creating custom services
    * 
    * */
   
   angular.module('achieveApp.common',['ngResource','ui.bootstrap']);
    
    
})();