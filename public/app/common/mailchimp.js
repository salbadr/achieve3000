(function() {
    'use strict';

    /**
     * 
     * Handles submission to the mailChimp API
     * 
     * */
    var common = angular.module('achieveApp.common');

    common.factory('mailChimp', ['$resource', 'MAIL_CHIMP_API_KEY', 'MAIL_CHIMP_LIST_ID', 'MAIL_CHIMP_API_URL',
        function mailChimp($resource, MAIL_CHIMP_API_KEY, MAIL_CHIMP_LIST_ID, MAIL_CHIMP_API_URL) {
            
            /**
             * 
             * The public API
             * 
             **/
            var api = {
                subscribeUser: subscribeUser
            }

            return api;

            /**
             * 
             * Prepares the user data for submission. A private function used by subscribeUSer
             * 
             * @param Object user The user to be subscribed
             * @returns Object A Json Object in the format accepted by mailChimp API
             * 
             * */
            function prepareData(user) {
                var struct = {
                    apikey: MAIL_CHIMP_API_KEY,
                    id: MAIL_CHIMP_LIST_ID,
                    email: {
                        email: user.emailaddress
                    },
                    merge_vars: {
                        FNAME: user.firstname,
                        LNAME: user.lastname
                    }

                }

                return struct;

            }

            /**
             * 
             * Subscribes User using the mailChimp API
             * Expects a user object and prepares it for submission
             * 
             * @param Object user The user to be subscribed
             * @return Object A promise whether it was successful or not
             * 
             * */
            function subscribeUser(user) {
            
                var resource = $resource(MAIL_CHIMP_API_URL + 'lists/subscribe.json', null);
                return resource.save(JSON.stringify(prepareData(user)));
            }

        }
    ]);

})();