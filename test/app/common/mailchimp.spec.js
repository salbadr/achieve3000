'use strict';



describe('mailChimp', function() {

   var mailChimp,
      user = {
         firstname: 'Salman',
         lastname: 'Badr',
         emailaddress: 'salmanbadr@gmail.com'
      };


   beforeEach(module('achieveApp.common'));


   mailChimp = {
      prepareData: function(data) {
         var struct = {
            apikey: '7b6b7220d75c52bbed85c4114e6755b7',
            id: '7c7be738af',
            email: {
               email: user.emailaddress
            },
            merge_vars: {
               FNAME: user.firstname,
               LNAME: user.lastname
            }

         }

         return struct;
      }
   }


   describe('prepareData', function() {

      it('should have the email Address submitted', function() {
         var data = mailChimp.prepareData(user);
         expect(data.email.email).toEqual(user.emailaddress);

      });
      it('should have the first name submitted', function() {
         var data = mailChimp.prepareData(user);
         expect(data.merge_vars.FNAME).toEqual(user.firstname);

      });
      it('should have the last name submitted', function() {
         var data = mailChimp.prepareData(user);
         expect(data.merge_vars.LNAME).toEqual(user.lastname);

      });
   });


describe('subscribeUser', function() {
   var $httpBackend,
      mailChimp,
      user = {
         firstname: 'Salman',
         lastname: 'Badr',
         emailAddress: 'salmanbadr@gmail.com'
      };

   beforeEach(module('achieveApp.common'));

   beforeEach(module('achieveApp.newsletter'));

   beforeEach(function() {
      module(function($provide) {
         $provide.value('MAIL_CHIMP_API_KEY', '7b6b7220d75c52bbed85c4114e6755b7');
         $provide.value('MAIL_CHIMP_LIST_ID', '7c7be738af');
         $provide.value('MAIL_CHIMP_API_URL', 'https://us4.api.mailchimp.com/2.0/');

      });
   });



   beforeEach(inject(function($injector) {
      $httpBackend = $injector.get('$httpBackend');
      mailChimp = $injector.get('mailChimp');



   }));

   afterEach(function() {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
   });




   it('should respond with error in case of failure of submission to Mailchimp API', function() {



      var error_obj = {
         status: 'error',
         code: -99,
         name: 'Unknown_Exception',
         error: 'An unknown error occurred processing your request.  Please try again later.'
      }
      $httpBackend.expectPOST('https://us4.api.mailchimp.com/2.0/lists/subscribe.json', function(data) {
         return true;
      }).respond(401, JSON.stringify(error_obj));


      mailChimp.subscribeUser(user).$promise.
      catch(function(response) {
         expect(response.data.status).toEqual('error');
      })

      $httpBackend.flush();

   });

   it('should respond with the email address added in case of submission to Mailchimp API', function() {

      var success_obj = {
         email: user.emailAddress
      }

      $httpBackend.expectPOST('https://us4.api.mailchimp.com/2.0/lists/subscribe.json', function(data) {
         return true;
      }).respond(201, JSON.stringify(success_obj));


      mailChimp.subscribeUser(user).$promise.
      then(function(response) {
         expect(response.email).toEqual(user.emailAddress);
      })

      $httpBackend.flush();
   })

});
});
